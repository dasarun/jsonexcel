import xlrd
import json

loc = ("australia.xlsx")

data = "["

streamcount = 0


def main():
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    totalrow = sheet.nrows

    data = {}

    for row in range(totalrow):
        currData = data
        for value in sheet.row_values(row):
            if value not in currData:
                currData[value] = {}
            currData = currData[value]

    jsonVal = json.dumps(data)

    print(jsonVal)
    f = open("file.json", "w")
    f.write(jsonVal)



if __name__ == '__main__':
    main()
